/*
 * Paths
 */
const THEME_PATH = './'

const STYLE_SRC = `${THEME_PATH}/src/styles/index.styl`
const STYLE_WATCH = `${THEME_PATH}/src/styles/**/*.styl`
const STYLE_DEST = `${THEME_PATH}/build/css`

const SCRIPT_SRC = `${THEME_PATH}/src/scripts/**/*.js`
const SCRIPT_DEST = `${THEME_PATH}/build/js`

// common dependencies
const gulp = require('gulp')
const connect = require('gulp-connect')

// css dependencies
const sourcemaps = require('gulp-sourcemaps')
const stylus = require('gulp-stylus')
const rupture = require('rupture')
const autoprefixer = require('autoprefixer-stylus')
const koutoSwiss = require('kouto-swiss')
const cmq = require('gulp-combine-mq')

// js dependencies
const babel = require('gulp-babel')
const concat = require('gulp-concat')

gulp.task('connect', () => {
  connect.server({ livereload: true })
})

gulp.task('css', () =>
  gulp.src(STYLE_SRC)
    .pipe(sourcemaps.init())
    .pipe(stylus({
      use: [rupture(), autoprefixer(), koutoSwiss()],
      compress: true,
      include: [
        './node_modules/../',
        './node_modules/s-grid/',
      ]
    }))
    .pipe(cmq({beautify: false}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(STYLE_DEST))
    .pipe(connect.reload())
)

gulp.task('js', () =>
    gulp.src(SCRIPT_SRC)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env', 'es2017']
        }))
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(SCRIPT_DEST))
        .pipe(connect.reload())
)

gulp.task('watch', () => {
  gulp.watch(STYLE_WATCH, ['css'])
  gulp.watch(SCRIPT_SRC, ['js'])
})

gulp.task('default', ['connect', 'js', 'css', 'watch'])
