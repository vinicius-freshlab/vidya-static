(($) => {
  $(document).ready(() => {
    $('.single-slider').slick({
      dots: false,
      prevArrow: '<button type="button" class="slider-navigation-button prev-button">&lt;</button>',
      nextArrow: '<button type="button" class="slider-navigation-button next-button">&gt;</button>',
    })
  })
})(jQuery)
